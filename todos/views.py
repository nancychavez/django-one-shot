from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}

    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    context = {"todo_list": todo_list}

    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    form = TodoListForm()
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()

            return redirect("todo_list_detail", id=list.id)

    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)
    form = TodoListForm(instance=todo_list)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            update_list = form.save()

        return redirect("todo_list_detail", update_list.id)

    context = {"edit_form": form}
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    context = {"delete": todo_list}
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    form = TodoItemForm()
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()

        return redirect("todo_list_detail", todo_item.list.id)

    context = {"form": form}

    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    todo_item = TodoItem.objects.get(id=id)
    form = TodoItemForm(instance=todo_item)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item = form.save()

        return redirect("todo_list_detail", todo_item.list.id)

    context = {"form": form}

    return render(request, "todos/update_item.html", context)
